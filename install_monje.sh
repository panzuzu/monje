#!/bin/bash
################################################
#          Instalación Monje 1.0               #
#                                              #
#               by Panzuzu                     #
#      https://gitlab.com/panzuzu/monje        #
#          panzuzu.dev@gmail.com               #
################################################

# Colores
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
reset=`tput sgr0`

# Creamos directorio de instalación dentro de la carpeta local
if [ -d ~/.monje ] ;then
    echo "${red} [Error] ${reset}- Existe directorio. Ejecutar desinstalación de Monje. -> sh uninstall_monje.sh"
    exit
else
    echo "${yellow} [Warning] ${reset}- Creando directorio..."
    mkdir -p ~/.monje
    ABSDIR=$(dirname $(readlink -f $0))

    if cp -r $ABSDIR/data/. ~/.monje; then
        echo "${green} [Ok] ${reset}- Directorio creado"
    else
        rm -r ~/.monje
        echo "${yellow} [Warning] ${reset}- La instalación no se pudo completar."
        exit
    fi
fi

# Instalación de PIP para manejo de paquetes
if which pip >/dev/null; then
    echo "${green} [Ok] ${reset}- PIP Instalado"
else
    echo "${yellow} [Warning] ${reset}- PIP no está instalado. Instalando..."
    if sudo apt -y update && sudo apt -y install python3-pip; then
        echo "${green} [Ok] ${reset}- PIP Instalado"
    else
        echo "${red} [ERROR] ${reset}- No se instaló PIP"
        rm -r ~/.monje
        echo "${yellow} [Warning] ${reset}- La instalación no se pudo completar."
        exit
    fi
fi

# Instalación de Python-VENV para entorno virtual
if python3 -m venv -h  > /dev/null 2>&1; then
    echo "${green} [Ok] ${reset}- Python-venv instalado"
else
    echo "${yellow} [Warning] ${reset}- Python-venv no está instalado. Instalando..."
    if sudo apt-get -y install python3-venv; then
        echo "${green} [Ok] ${reset}- Python-venv instalado"
    else
        if sudo apt-get -y install python3.8-venv; then
            echo "${green} [Ok] ${reset}- Python-venv instalado"
        else
            echo "${red} [ERROR] ${reset}- No se pudo instalar Python-venv. Se cancela la instalación"
            rm -r ~/.monje
            echo "${yellow} [Warning] ${reset}- La instalación no se pudo completar."
            exit
        fi
    fi
fi

# Nos situamos en la carpeta raiz del programa
cd ~/.monje
pwd

# Creamos un entorno virtual ENV
if python3 -m venv env; then
    echo "${green} [Ok] ${reset}- Entorno virtual creado"
else
    echo "${red} [Error] ${reset}- No se pudo crear el entorno virtual"
    rm -r ~/.monje
    rm $HOME/.local/share/applications/monje.desktop
    echo "${yellow} [Warning] ${reset}- La instalación no se pudo completar."
    exit
fi

# Ejecutamos el entorno virtual ENV
if [ -d env/ ];then
    if . env/bin/activate;then
        echo "${green} [Ok] ${reset}- Corriendo entorno virtual"
    else
        echo "${red} [Error] ${reset}- No pudo correr entorno virtual"
        rm -r ~/.monje
        rm $HOME/.local/share/applications/monje.desktop
        echo "${yellow} [Warning] ${reset}- La instalación no se pudo completar."
        exit
    fi
else
    exit
fi

# Instalamos ffmpeg en ENV
if which ffmpeg >/dev/null; then
    echo "${green} [Ok] ${reset}- ffmpeg librería instalada"
else
    echo "${yellow} [Warning] ${reset}- ffmpeg no está instalado. Instalando..."
    if sudo apt install ffmpeg; then
        echo "${green} [Ok] ${reset}- ffmpeg librería intalada"
    else
        echo "${red} [ERROR] ${reset}- No se instaló la librería ffmpeg"
        rm -r ~/.monje
        rm $HOME/.local/share/applications/monje.desktop
        echo "${yellow} [Warning] ${reset}- La instalación no se pudo completar."
        exit
    fi
fi

# Instalamos wheel en ENV
echo "${yellow} [Warning] ${reset}- Instalando wheel"
if pip install wheel; then
    echo "${green} [Ok] ${reset}- Paquete Wheel instalado"
else
    echo "${red} [Error] ${reset}- Ocurrió un [error] instalando Wheel"
    rm -r ~/.monje
    rm $HOME/.local/share/applications/monje.desktop
    echo "${yellow} [Warning] ${reset}- La instalación no se pudo completar."
    exit
fi

# Instalamos dependencias del programa en ENV
if pip install -r requirements.txt; then
    echo "${green} [Ok] ${reset}- Dependencias instaladas."
else
    echo "${red} [Error] ${reset}- Ocurrió un [error] instalando las dependencias"
    rm -r ~/.monje
    rm $HOME/.local/share/applications/monje.desktop
    echo "${yellow} [Warning] ${reset}- La instalación no se pudo completar."
    exit
fi

# Desactivamos entorno virtual ENV
if deactivate; then
    echo "${green} [Ok] ${reset}- Desactivado entorno"
else
    echo "${red} [Error] ${reset}- Ocurrió un [error] desactivando el entorno. Desactivar manualmente. \n Command: deactivate"
    rm -r ~/.monje
    rm $HOME/.local/share/applications/monje.desktop
    echo "${yellow} [Warning] ${reset}- El entorno no se ha podido desactivar. Puede ser que tenga problemas a futuro."
fi

# Creamos ícono para el programa, luego lo agregamos en .local/share/applications
if ! [ -d $HOME/.local/share/applications ];then
    mkdir -p $HOME/.local/share/applications
fi

if echo \
"[Desktop Entry]
Version=1.0
Exec=$HOME/.monje/monje.sh
Name=Monje
Comment=Programa para convertir audios a textos
Icon=$HOME/.monje/app_data/assets/img/monje.png
Encoding=UTF-8
Terminal=false
Type=Application
Categories=Utility;"\ > $HOME/.local/share/applications/monje.desktop; then
    chmod +x $HOME/.local/share/applications/monje.desktop
    echo "${green} [Ok] ${reset}- Acceso directo creado."
    echo "${green} [Ok] ${reset}- Instalación completa."
else
    echo "${red} [Error] ${reset}- Error en la instalación. Eliminando archivos..."
    rm -r ~/.monje
    rm $HOME/.local/share/applications/monje.desktop
    echo "${yellow} [Warning] ${reset}- La instalación no se pudo completar."
    exit
fi