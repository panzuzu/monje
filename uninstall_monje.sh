#!/bin/bash
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
reset=`tput sgr0`
count_errors=0

if ! [ -d ~/.monje ] ;then
    echo "${yellow} [Warning] ${reset}- No existe directorio."
else
    if sudo rm -r $HOME/.monje;then
        echo "${green} [Ok] ${reset}- Directorio eliminado: $HOME/.monje"
    else
        echo "${red} [Error] ${reset}- No se pudo eliminar directorio: $HOME/.monje"
        count_errors=$((count_errors+1))
    fi
fi

if ! [ -f $HOME/.local/share/applications/monje.desktop ] ;then
    echo "${yellow} [Warning] ${reset}- No existe acceso directo."
else
    if sudo rm $HOME/.local/share/applications/monje.desktop;then
        echo "${green} [Ok] ${reset}- Acceso directo eliminado: $HOME/.local/share/applications/monje.desktop"
    else
        echo "${red} [Error] ${reset}- No se pudo eliminar el acceso directo: $HOME/.local/share/applications/monje.desktop"
        count_errors=$((count_errors+1))
    fi
fi

echo "Total de errores $count_errors"
if [ ${count_errors} -eq 0 ];then
    echo "${green} [Ok] ${reset}- Monje ha sido desinstalado"
else
    echo "${red} [Error] ${reset}- Monje no ha podido desinstalarse"
fi