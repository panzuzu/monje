# Monje

Monje es un programa para convertir archivos de audio (entrevistas, charlas, reuniones, etc) en texto.

## Requerimientos

El programa está creado con:

- PySide2 : 5.15.0
- SpeechRecognition : 3.8.1
- pydub : 0.25.1
- wheel : 0.37.0
- pathlib : 1.0.1

## Instalación

Instalar virtualenv

Para esto ingresamos:

```
sudo apt-get -y update
sudo apt-get -y install python3-venv ó sudo apt-get -y install python3.8-venv
```

Instalamos ejecutando el script "install_monje.sh":

```
sh install_monje.sh
```

Esto nos agregará un acceso directo para la ejecución del programa. 
Podemos buscarlo en aplicaciones.


Para desinstalar el programa solo ejecutamos "uninstall_monje.sh":

```
sh uninstall_monje.sh
```


## Datos innecesarios

Monje guarda sus archivos para trabajar en la carpeta tmp/. Dentro de ella tenemos audio-chunks, donde se guardan los audios divididos, y convert-audios, donde son guardados los audios convertidos.

Para la conversión de audio a texto se utiliza la librería speechtoaudio de Google. Por lo que el programa debe estar conectado a internet para funcionar.

La funcionalidad para dividir audios por silencio fue extraída del siguiente [LINK](https://www.thepythoncode.com/article/using-speech-recognition-to-convert-speech-to-text-python ). Todos los atributos para aquella persona.

El programa está en desarrolo, es mi primer proyecto en pyside2. 
Cuálquier duda pueden contactarme a **panzuzu.dev@gmail.com.**



