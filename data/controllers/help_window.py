from PySide2.QtWidgets import QWidget
from PySide2.QtCore import Qt
from views.help_window import HelpWindow

class HelpWindow(QWidget, HelpWindow):

    def __init__(self, parent=None):
        super().__init__(parent)

        self.setupUi(self)
        self.setWindowFlag(Qt.Window)