from PySide2 import QtWidgets
from PySide2.QtWidgets import QMainWindow, QFileDialog
from views.main_window import MainWindow

from pydub import AudioSegment
from pydub.silence import split_on_silence
from pathlib import Path
import speech_recognition as sr
import os
import time

from components.list_status_items import ListStatusItems
from components.check_extensions import CheckExtensions
from components.message_boxes import MessageBoxes
from components.helpers import Helpers
from components.paths import Paths

#acceder a nombre de os (os.environ.get('OS_NAME')

class Main(QMainWindow, MainWindow):

    def __init__(self):
        super().__init__()

        self.setupUi(self)
        self.examinarButton.clicked.connect(self.select_file)
        self.examinarExportButton.clicked.connect(self.select_path)
        self.convertButton.clicked.connect(self.convert_to_text)
        self.actionAyuda.triggered.connect(self.ventana_ayuda)
        self.actionSalir.triggered.connect(self.exit)

    def exit(self):
        quit()
    
    def ventana_ayuda(self):
        from controllers.help_window import HelpWindow
        window = HelpWindow(self)
        window.show()
        pass

    def select_file(self):
        file_path = QFileDialog.getOpenFileName()[0]
        self.pathLineText.setText(file_path)
        path = self.pathLineText.text()
        lista = self.progressListWidget

        if not CheckExtensions.audio(path):
            self.pathLineText.clear()
            MessageBoxes.warning(self,'Archivo no permitido','No se acepta este formato para convertir')
        else:
            if not path.endswith('.wav'):
                Helpers.convert_audio(self, path, lista)

    def select_path(self):
        file_path = str(QFileDialog.getExistingDirectory(self, "Seleccioná la carpeta"))
        self.pathExportLineEdit.setText(file_path)

    def convert_to_text(self):
        lista = self.progressListWidget
        path = self.pathLineText.text()
        export_path = self.pathExportLineEdit.text()

        if path == '':
            MessageBoxes.warning(self,'No hay archivo','Debe ingresar un archivo para convertir')
            return False
        if export_path == '':
            MessageBoxes.warning(self,'No hay carpeta de destino','Debe ingresar la carpeta de destino en donde se van a guardar los textos')
            return False


        lista.insertItem(0, ListStatusItems.monje_item("Iniciando mi escritura"))

        self.progressBar.setValue(0)
        self.convertButton.setEnabled(False)
        
        r = sr.Recognizer()
        # Abrimos audio y segmentamos mediante los silencios
        sound = AudioSegment.from_wav(path)
        chunks = split_on_silence(sound,
            # Valor de reconocimiento de silenci ** modificable **
            min_silence_len = 500,
            # Valor de ruido ** modificable **
            silence_thresh = sound.dBFS-14,
            # Espera de silencio ** modificable **
            keep_silence=500,
        )
        
        whole_text = ""
        # Generación de fragmentos de audios
        self.progressBar.setValue(35)
        for i, audio_chunk in enumerate(chunks, start=1):
            # Se crean los audios en la carpeta
            chunk_filename = os.path.join(Paths.audio_chunks(), f"chunk{i}.wav")
            audio_chunk.export(chunk_filename, format="wav")
            # Utilizamos speech_recognition para reconocer el audio
            with sr.AudioFile(chunk_filename) as source:
                audio_listened = r.record(source)
                # Convertimos a texto, mostramos por pantalla cada resultado
                # Agregamos el texto obtenido a la variable whole_text
                # Error: se muestra la exception
                try:
                    text = r.recognize_google(audio_listened, language="es-ES")
                    QtWidgets.QApplication.processEvents()
                except sr.UnknownValueError:
                    lista.insertItem(0, ListStatusItems.error_item("No se reconoce el audio"))
                except sr.RequestError:
                    lista.insertItem(0, ListStatusItems.error_item("Problemas de conectividad con servidor de Google"))
                else:
                    text = f"{text.capitalize()}. "
                    lista.insertItem(0, ListStatusItems.success_item(text))
                    whole_text += text
        # Obtenemos el nombre del archivo a convertir
        name_path = Path(path).stem

        # Creamos archivo de texto. 
        # Si encontramos un archivo con el mismo nombre agregamos milisegundos
        if not os.path.isfile('%s/%s_audiototext.txt' % (export_path, name_path)):
            f = open ('%s/%s_audiototext.txt' % (export_path, name_path),'x')
        else:
            f = open ('%s/%s_audiototext_%s.txt' % (export_path, name_path, time.time_ns()),'x')
        f.write(whole_text)
        f.close()

        self.progressBar.setValue(100)
        self.convertButton.setEnabled(True)

        lista.insertItem(0, ListStatusItems.monje_item("Ordenando los papeles que dejé dispersos."))
        Helpers.clean_temp_directories()
        # Retornamos el nombre del archivo texto generado
        lista.insertItem(0, ListStatusItems.monje_item("Terminé de transcribir el audio. Gracias por confiar en mí."))
        success_text = "Archivo guardado en %s" % export_path
        lista.insertItem(0, ListStatusItems.download_item(success_text))
        MessageBoxes.info(self, 'Texto escrito correctamente', success_text)
        return f.name