# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'help_window.ui'
##
## Created by: Qt User Interface Compiler version 5.15.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
    QPixmap, QRadialGradient)
from PySide2 import QtCore
from PySide2.QtWidgets import *

import app_data.assets.assets_rc

class HelpWindow(object):
    def setupUi(self, HelpWindow):
        if not HelpWindow.objectName():
            HelpWindow.setObjectName(u"HelpWindow")
        HelpWindow.resize(400, 262)
        HelpWindow.setMaximumSize(QtCore.QSize(400, 262))
        HelpWindow.setMinimumSize(QtCore.QSize(400, 262))
        HelpWindow.setWindowFlags(QtCore.Qt.WindowCloseButtonHint | QtCore.Qt.WindowMinimizeButtonHint) 
        self.verticalLayoutWidget = QWidget(HelpWindow)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(10, 10, 381, 85))
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.label_2 = QLabel(self.verticalLayoutWidget)
        self.label_2.setObjectName(u"label_2")

        self.verticalLayout.addWidget(self.label_2)

        self.verticalLayoutWidget_2 = QWidget(HelpWindow)
        self.verticalLayoutWidget_2.setObjectName(u"verticalLayoutWidget_2")
        self.verticalLayoutWidget_2.setGeometry(QRect(10, 110, 381, 141))
        self.verticalLayout_2 = QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.label = QLabel(self.verticalLayoutWidget_2)
        self.label.setObjectName(u"label")

        self.verticalLayout_2.addWidget(self.label)


        self.retranslateUi(HelpWindow)

        QMetaObject.connectSlotsByName(HelpWindow)
    # setupUi

    def retranslateUi(self, HelpWindow):
        HelpWindow.setWindowTitle(QCoreApplication.translate("HelpWindow", u"Ayuda", None))
        self.label_2.setText(QCoreApplication.translate("HelpWindow", u"<html><head/><body><p align=\"center\"><img src=\":/logos/img/logo.png\"/></p></body></html>", None))
        self.label.setText(QCoreApplication.translate("HelpWindow", u"<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">Programa creado por Panzuzu</span></p><p align=\"center\"><span style=\" font-style:italic;\">v1.0</span></p><p align=\"center\"><span style=\" color:#555753;\">panzuzu.dev@gmail.com</span></p></body></html>", None))
    # retranslateUi

