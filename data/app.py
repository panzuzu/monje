from PySide2.QtWidgets import QApplication
from controllers.main_window import Main
import os
import platform


if __name__ == "__main__":
    app = QApplication()
    window = Main()
    window.show()
    
    os.environ['OS_NAME'] = platform.system()

    app.exec_()