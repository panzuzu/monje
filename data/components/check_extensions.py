from PySide2.QtWidgets import QMessageBox
import pathlib

from components.message_boxes import MessageBoxes

class CheckExtensions:
    def audio(path):
        print (path)
        extensiones_permitidas =  [".mp3", ".m4a", ".ogg", ".wav"]
        if path != '' and pathlib.Path(path).suffix in extensiones_permitidas:
            return True
        else:
            return False