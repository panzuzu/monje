from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QListWidgetItem
from components.paths import Paths

class ListStatusItems:
    def error_item(text):
        item = QListWidgetItem()
        item.setIcon(QIcon(Paths.error_icon()))
        item.setText(text)
        return item

    def success_item(text):
        item = QListWidgetItem()
        item.setIcon(QIcon(Paths.success_icon()))
        item.setText(text)
        return item

    def warning_item(text):
        item = QListWidgetItem()
        item.setIcon(QIcon(Paths.warning_icon()))
        item.setText(text)
        return item

    def monje_item(text):
        item = QListWidgetItem()
        item.setIcon(QIcon(Paths.monje_icon()))
        item.setText(text)
        return item

    def download_item(text):
        item = QListWidgetItem()
        item.setIcon(QIcon(Paths.download_icon()))
        item.setText(text)
        return item