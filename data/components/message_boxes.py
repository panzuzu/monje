from PySide2.QtWidgets import QMessageBox

class MessageBoxes:
    def warning(obj,title,text):
        messagebox = QMessageBox(QMessageBox.Warning , title, text, parent=obj)
        messagebox.exec_()

    def info(obj,title,text):
        messagebox = QMessageBox(QMessageBox.Information , title, text, parent=obj)
        messagebox.exec_()