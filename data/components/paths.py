class Paths:
    def error_icon(*args):
        return './app_data/assets/img/error.png'
    def success_icon(*args):
        return './app_data/assets/img/success.png'
    def warning_icon(*args):
        return './app_data/assets/img/warning.png'
    def monje_icon(*args):
        return './app_data/assets/img/monje.png'
    def download_icon(*args):
        return './app_data/assets/img/download.png'
    def convert_icon(*args):
        return './app_data/assets/img/convert.png'
    def tmp(*args):
        return './app_data/tmp'
    def assets(*args):
        return './app_data/assets'
    def audio_chunks(*args):
        route = f'%s/audio-chunks' % Paths.tmp()
        return route
    def convert_audios(*args):
        route = f'%s/convert-audios' % Paths.tmp()
        return route
