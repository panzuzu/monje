from components.paths import Paths
from components.list_status_items import ListStatusItems
from pydub import AudioSegment
import os
import time

class Helpers:
    def clean_temp_directories(*args):
        dir = Paths.audio_chunks()
        for f in os.listdir(dir):
            os.remove(os.path.join(dir, f))
        dir = Paths.convert_audios()
        for f in os.listdir(dir):
            os.remove(os.path.join(dir, f))
    def convert_audio(obj, path, lista):
        obj.convertButton.setEnabled(False)
        texto = "Convirtiendo archivo"
        lista.insertItem(0, ListStatusItems.warning_item(texto))
        dst = "%s/%s.wav" % (Paths.convert_audios(),time.time_ns())
        audSeg = AudioSegment.from_file(path)
        audSeg.export(dst, format="wav")
        lista.insertItem(0, ListStatusItems.monje_item(str(dst)))
        path = dst
        obj.convertButton.setEnabled(True)
        if path.endswith('.wav'):
            lista.insertItem(0, ListStatusItems.success_item("Formato de archivo aceptado."))
            lista.insertItem(0, ListStatusItems.monje_item("Puede convertir el archivo a texto."))
            obj.pathLineText.setText(path)
            return True
        else:
            lista.insertItem(0, ListStatusItems.error_item("Formato de archivo no soportado."))
            return False